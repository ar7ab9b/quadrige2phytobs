--- export surval emodnet phytobs 
-- 08/11/2019 GD pour AR

desc P_30200_ENTITY ;

select * from P_30200_ENTITY
where substr(ENT_NM,1,9) in (
'002-P-007', '006-P-001', '010-P-109', '022-P-018', '027-P-028', '040-P-017', 
'047-P-016', '055-P-001', '063-P-002', '079-P-026', '082-P-001', '088-P-050', 
'095-P-002', '097-P-002', '104-P-001', '102-P-007', '114-P-058'
);

desc p_30200_result ;

select count(*) 
from p_30200_result
where ent_id in (2003002,
3006001,
6012008,
12025037,
14029050,
19039017,
21043016,
25049001,
27057005,
31068003,
32071002,
34077061,
36081002,
36083002,
37087001,
37088003,
41109006)
;


select count(*) 
from p_30200_result r
inner join P_30200_PARAMETER p on p.PAR_CD = r.PAR_CD
inner join P_30200_MATRIX mat on mat.MATRIX_ID = r.MATRIX_ID
left outer join P_30200_TAXON t on t.TAXON_ID = r.TAXON_ID
inner join P_30200_THEME th on th.THEME_ID = r.THEME_ID
inner join P_30200_ENTITY e on e.ENT_ID = r.ENT_ID
where r.ent_id in (2003002,
3006001,
6012008,
12025037,
14029050,
19039017,
21043016,
25049001,
27057005,
31068003,
32071002,
34077061,
36081002,
36083002,
37087001,
37088003,
41109006)
;


select *
from p_30200_result r
inner join P_30200_PARAMETER p on p.PAR_CD = r.PAR_CD
inner join P_30200_MATRIX mat on mat.MATRIX_ID = r.MATRIX_ID
left outer join P_30200_TAXON t on t.TAXON_ID = r.TAXON_ID
inner join P_30200_THEME th on th.THEME_ID = r.THEME_ID
inner join P_30200_ENTITY e on e.ENT_ID = r.ENT_ID
where r.ent_id in (2003002,
3006001,
6012008,
12025037,
14029050,
19039017,
21043016,
25049001,
27057005,
31068003,
32071002,
34077061,
36081002,
36083002,
37087001,
37088003,
41109006)
;

--export r�sultats
select *
from p_30200_result r
inner join P_30200_PARAMETER p on p.PAR_CD = r.PAR_CD
inner join P_30200_MATRIX mat on mat.MATRIX_ID = r.MATRIX_ID
left outer join P_30200_TAXON t on t.TAXON_ID = r.TAXON_ID
inner join P_30200_THEME th on th.THEME_ID = r.THEME_ID
inner join P_30200_ENTITY e on e.ENT_ID = r.ENT_ID
where r.ent_id in (2003002,
3006001,
6012008,
12025037,
14029050,
19039017,
21043016,
25049001,
27057005,
31068003,
32071002,
34077061,
36081002,
36083002,
37087001,
37088003,
41109006)
;


--export r�sultats juste abondance SDBIOL01
select *
from p_30200_result r
inner join P_30200_PARAMETER p on p.PAR_CD = r.PAR_CD
inner join P_30200_MATRIX mat on mat.MATRIX_ID = r.MATRIX_ID
left outer join P_30200_TAXON t on t.TAXON_ID = r.TAXON_ID
inner join P_30200_THEME th on th.THEME_ID = r.THEME_ID
inner join P_30200_ENTITY e on e.ENT_ID = r.ENT_ID
where r.ent_id in (2003002,
3006001,
6012008,
12025037,
14029050,
19039017,
21043016,
25049001,
27057005,
31068003,
32071002,
34077061,
36081002,
36083002,
37087001,
37088003,
41109006)
and  r.par_cd = 'SDBIOL01'
; --Phytoplancton

select * from P_30200_THEME ;
--date max par lieu - export r�sultats juste th�mes ('Phytoplancton', 'Hydrologie')
select r.ent_id, e.ENT_NM, max(r.result_dt)
from p_30200_result r
inner join P_30200_PARAMETER p on p.PAR_CD = r.PAR_CD
inner join P_30200_MATRIX mat on mat.MATRIX_ID = r.MATRIX_ID
left outer join P_30200_TAXON t on t.TAXON_ID = r.TAXON_ID
inner join P_30200_THEME th on th.THEME_ID = r.THEME_ID
inner join P_30200_ENTITY e on e.ENT_ID = r.ENT_ID
where r.ent_id in (2003002,
3006001,
6012008,
12025037,
14029050,
19039017,
21043016,
25049001,
27057005,
31068003,
32071002,
34077061,
36081002,
36083002,
37087001,
37088003,
41109006)
and  th.THEME_NM in ('Phytoplancton', 'Hydrologie')
group by r.ent_id, e.ENT_NM
;

select * from P_30200_THEME ;
--export r�sultats juste th�mes ('Phytoplancton', 'Hydrologie')
select *
from p_30200_result r
inner join P_30200_PARAMETER p on p.PAR_CD = r.PAR_CD
inner join P_30200_MATRIX mat on mat.MATRIX_ID = r.MATRIX_ID
left outer join P_30200_TAXON t on t.TAXON_ID = r.TAXON_ID
inner join P_30200_THEME th on th.THEME_ID = r.THEME_ID
inner join P_30200_ENTITY e on e.ENT_ID = r.ENT_ID
where r.ent_id in (2003002,
3006001,
6012008,
12025037,
14029050,
19039017,
21043016,
25049001,
27057005,
31068003,
32071002,
34077061,
36081002,
36083002,
37087001,
37088003,
41109006)
and  th.THEME_NM in ('Phytoplancton', 'Hydrologie')
;
-- Toutes stations 
select * from P_30200_THEME ;
--export r�sultats juste th�mes ('Phytoplancton', 'Hydrologie')
select *
from p_30200_result r
inner join P_30200_PARAMETER p on p.PAR_CD = r.PAR_CD
inner join P_30200_MATRIX mat on mat.MATRIX_ID = r.MATRIX_ID
left outer join P_30200_TAXON t on t.TAXON_ID = r.TAXON_ID
inner join P_30200_THEME th on th.THEME_ID = r.THEME_ID
inner join P_30200_ENTITY e on e.ENT_ID = r.ENT_ID
where r.ent_id in (2003002,
3006001,
6012008,
12025001,
12025037,
14029001,
14029050,
19039001,
19039017,
21041003,
21043016,
25049001,
27057005,
31068003,
32071002,
34077061,
36081002,
36083002,
37087001,
37088003,
41109006)
and  th.THEME_NM in ('Phytoplancton', 'Hydrologie')
;


